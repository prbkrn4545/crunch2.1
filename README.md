create table clickTracker (ID int identity(1,1) ,customDomain varchar(30), CustomUrl varchar(300),VisitedUser varchar(20),CONSTRAINT PK_Click PRIMARY KEY (ID))

create table UrlMaster (ID int identity(1,1) , OriginalUrl varchar(300),AssignedUrl varchar(20),
AssignedOn date,ExpiresOn date,optedServer varchar(30),clicks int
,CONSTRAINT PK_Click PRIMARY KEY (ID))