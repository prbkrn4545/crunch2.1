export class UrlMaster {
    public id: number;
    public originalUrl: string;
    public assignedUrl: string;
    public assignedOn: string;
    public expiresOn: string;
    public optedServer : string;
    public clicks : number;
    UrlMaster() {
        this.id = 0;
        this.originalUrl = null;
        this.assignedUrl = null;
        this.assignedOn = null;
        this.expiresOn = null;
        this.optedServer = null;
        this.clicks = null;
    }
}
