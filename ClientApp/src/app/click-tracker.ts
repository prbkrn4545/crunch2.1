export class ClickTracker {
    public id: number;
    public customUrl: string;
    public visitedUser: string;
    
    constructor()
    {
        this.id = null;
        this.customUrl = null;
        this.visitedUser = null;
    }
}
