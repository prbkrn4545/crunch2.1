import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-qrcodegenerator',
  templateUrl: './qrcodegenerator.component.html',
  styleUrls: ['./qrcodegenerator.component.css']
})
export class QrcodegeneratorComponent implements OnInit {
  myAngularxQrCode: string;
  visible : boolean =false;
 
  constructor( ) {
    this.myAngularxQrCode = ' ';
    
  }
  ngOnInit() {
  }

  change()
  {
    this.visible = true;
  }

  onChange(event: any) {
    this.visible = true;
};

} 
