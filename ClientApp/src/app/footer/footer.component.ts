import { Component, OnInit } from '@angular/core';
import { IpServiceService } from '../ip-service.service';


@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
ip :string;
  constructor(private ipservice : IpServiceService) { }

  ngOnInit() {

    this.ipservice.getIPAddress().subscribe(
      (res:any)=>{  
      this.ip=res.ip;  
    },
    (err)=>{ 
      alert(err);
    }
    
    );  
  }

}
