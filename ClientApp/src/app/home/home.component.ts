//  ngrok http https://localhost:5001 -host-header="localhost:5001" 

import { Component, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm, FormControl } from '@angular/forms';
import { ReposService } from '../repos.service';
import { UrlMaster } from '../url-master';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  title = 'Change your URL..';
  private txtorigin: FormControl;
  private txtassigned: FormControl;
  private server: FormControl;
  date: Date = new Date();
  result = "";
  Url: UrlMaster = new UrlMaster();
  public flag: boolean;
  MainForm: FormGroup;
  alteredURL: string;
  servers: any = ['--Select--', 'teamtravellers.com'];
  show: boolean = false;
  public headertext: string = "Shorten URL";
  loadingmessage = "Loading..";
  public random: string = null;
  myAngularxQrCode :string =null;

  constructor(private repos: ReposService, public fb: FormBuilder, private cdr: ChangeDetectorRef, private toastr: ToastrService, private SpinnerService: NgxSpinnerService) { }

  ngAfterViewInit() {
    this.cdr.detectChanges();
    this.SpinnerService.hide();
  }
  ngOnInit() {
    this.SpinnerService.show();

    this.MainForm = this.fb.group({
      txtorigin: ['', Validators.required],
      txtassigned: ['', [Validators.required, Validators.minLength(3)]],
      server: ['', Validators.required],
    })
  }

  copytext() {
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = this.result;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.toastr.success("Ready to Paste..");
  }

  onSubmit() {
    this.loadingmessage = "Checking on Availability of your Desired Link... ";
    this.SpinnerService.show();

    if (this.MainForm.get('txtassigned').value == '' || this.MainForm.get('txtassigned').value.length == 0) {
      this.random = this.makeid(6);

      this.repos.isExistingURL(this.random,this.MainForm.get('server').value);

      this.repos.getValue().subscribe((value) => {
        this.flag = value;
      });

      setTimeout(() => {
        
          this.SpinnerService.hide();

          if (this.MainForm.get('txtorigin').value.includes('https://')) {
            this.alteredURL = this.MainForm.get('txtorigin').value.replace("https://", "");
          }
          else if (this.MainForm.get('txtorigin').value.includes('http://')) {
            this.alteredURL = this.MainForm.get('txtorigin').value.replace("http://", "");
          }
          else {
            this.alteredURL = this.MainForm.get('txtorigin').value;
          }
          this.Url.originalUrl = this.alteredURL;
          this.Url.assignedUrl = this.random;
          this.Url.assignedOn = Date.now().toLocaleString();
          this.date.setDate(this.date.getDate() + 3);
          this.Url.expiresOn = this.date.toDateString();
          this.Url.optedServer = this.MainForm.get('server').value;
          this.Url.clicks = 0;
          console.log('first if'+this.Url);
          console.log(this.Url);
          this.repos.insert(this.Url).subscribe(
            (reponse) => {
              this.toastr.success("Short URL Created Successfully");
              this.headertext = " Requested Shorten URL";
              this.result = "http://" + this.MainForm.get('server').value + "/" + this.random;
              this.show = true;
              this.MainForm.reset();
              this.myAngularxQrCode = this.result;
              this.MainForm.get('server').setValue("--Select--");
            },
            (err) => { console.log(err) }
          );
      }, 7000);

      
    }
    else {
      this.repos.isExistingURL(this.MainForm.get('txtassigned').value,this.MainForm.get('server').value);

      this.repos.getValue().subscribe((value) => {
        this.flag = value;
      });

      setTimeout(() => {
        if (this.flag) {
          this.toastr.error("Desired Link Not Available...Please Make aslight changes and Try Again...");
          this.SpinnerService.hide();
        }
        else {
          this.SpinnerService.hide();

          if (this.MainForm.get('txtorigin').value.includes('https://')) {
            this.alteredURL = this.MainForm.get('txtorigin').value.replace("https://", "");
          }
          else if (this.MainForm.get('txtorigin').value.includes('http://')) {
            this.alteredURL = this.MainForm.get('txtorigin').value.replace("http://", "");
          }
          else {
            this.alteredURL = this.MainForm.get('txtorigin').value;
          }
          this.Url.originalUrl = this.alteredURL;
          this.Url.assignedUrl = this.MainForm.get('txtassigned').value;
          this.Url.assignedOn = Date.now().toLocaleString();
          this.date.setDate(this.date.getDate() + 3);
          this.Url.expiresOn = this.date.toDateString();
          this.Url.optedServer = this.MainForm.get('server').value;
          this.Url.clicks = 0;
          this.repos.insert(this.Url).subscribe(
            (reponse) => {
              this.toastr.success("Short URL Created Successfully");
              this.headertext = " Requested Shorten URL";
              this.result = "http://" + this.MainForm.get('server').value + "/" + this.MainForm.get('txtassigned').value;
              this.show = true;
              this.myAngularxQrCode = this.result;
              this.MainForm.reset();
              this.MainForm.get('server').setValue("--Select--");
            },
            (err) => { console.log(err) }
          );
        }
      }, 7000);

     
    }

  }

  makeid(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }


}
