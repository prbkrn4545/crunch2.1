import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClickstrackerComponent } from './clickstracker.component';

describe('ClickstrackerComponent', () => {
  let component: ClickstrackerComponent;
  let fixture: ComponentFixture<ClickstrackerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClickstrackerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClickstrackerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
