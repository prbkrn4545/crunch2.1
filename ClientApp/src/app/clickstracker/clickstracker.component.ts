import { Component, OnInit } from '@angular/core';
import { ReposService } from '../repos.service';
import { UrlMaster } from '../url-master';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ClickTracker } from '../click-tracker';


@Component({
  selector: 'app-clickstracker',
  templateUrl: './clickstracker.component.html',
  styleUrls: ['./clickstracker.component.css']
})
export class ClickstrackerComponent implements OnInit {
  urlm: UrlMaster;
  count: string;
  visible: boolean = false;
  alteredURL: string;
  MainForm: FormGroup;
  private txtlink: FormControl;
  visitors  : ClickTracker[];
  uniquecount : string;

  constructor(private repos: ReposService, public fb: FormBuilder, private toastr: ToastrService, private SpinnerService: NgxSpinnerService) { }

  ngOnInit() {
    this.SpinnerService.hide();

    this.MainForm = this.fb.group({
      txtlink: ['', Validators.required]
    })
  }

  onSubmit() {
    this.SpinnerService.show();
    if (this.MainForm.get('txtlink').value.includes('https://')) {
      this.alteredURL = this.MainForm.get('txtlink').value.replace("https://", "");
    }
    else if (this.MainForm.get('txtlink').value.includes('http://')) {
      this.alteredURL = this.MainForm.get('txtlink').value.replace("http://", "");
    }
    else {
      this.alteredURL = this.MainForm.get('txtlink').value;
    }
    let x = this.alteredURL.split("/");
    if (x.length > 0) {


      this.repos.getcount(x[1],x[0]).subscribe(
        (response: UrlMaster) => {
          this.urlm = response;
          this.visible = true;
          this.count = this.urlm[0].clicks;

          this.repos.getVisitor(x[1],x[0]).subscribe(
            (response : ClickTracker [])=>{
              // this.visitors = response;
              this.uniquecount = response.length.toString();
            }
          );

          this.SpinnerService.hide();
        },
        (err) => {
          this.SpinnerService.hide();
          this.toastr.show('Link Cannot be Found or might have been expired..');
        }
      );
    }
    else {
      this.SpinnerService.hide();
      this.toastr.show('Link Cannot be Processed');
    }

  }
}
