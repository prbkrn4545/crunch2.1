import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { UrlMaster } from './url-master';
import { ClickTracker } from './click-tracker';
import { Usermodel } from './usermodel';

@Injectable({
  providedIn: 'root'
})
export class ReposService {

  private isexisting: BehaviorSubject<boolean>;

  constructor(private http : HttpClient) {
    this.isexisting = new BehaviorSubject<boolean>(false);
    
   }

  flag : boolean ;
  url: string;
  isExistingURL(link : string,domain : string) 
  {
    //debugger;
      this.http.get('/api/URL/'+link+'/'+domain).subscribe(
        (response : boolean) => {
            this.setValue(response);
        },
        (err) => {
          console.log(err);
          
        }
      );
      return this.flag;
  }

  urlm : UrlMaster = new UrlMaster();

  getoriginalURL(link : string,domain : string) : Observable<UrlMaster>
  {
    return this.http.get<UrlMaster>('/api/URL/'+link+'/'+domain+'/1/1');
  }

  getValue(): Observable<boolean> {
    return this.isexisting.asObservable();
  }

  setValue(newValue): void {
    this.isexisting.next(newValue);
  }

  insert(url : UrlMaster )  
  {
    console.log(url);
    const header = new HttpHeaders()
     .set('Content-type', 'application/json');
    let options = { headers: header };
    return this.http.post('/api/URL',url,options);
  }

  getcount(link : string,domain: string) : Observable<UrlMaster>
  {
    return this.http.get<UrlMaster>('/api/URL/'+link+"/"+domain+"/1");
  }

  getVisitor(link : string,domain: string) : Observable<ClickTracker[]>
  {
    return this.http.get<ClickTracker[]>('/api/Clicks/'+link+"/"+domain+'');
  }

  addvisitor(ct : ClickTracker)
  {
    const header = new HttpHeaders()
    .set('Content-type', 'application/json');
   let options = { headers: header };
    return this.http.post('/api/Clicks',ct,options);
  }

  authenticate(um : Usermodel)
  {
    const header = new HttpHeaders()
    .set('Content-type', 'application/json');
   let options = { headers: header };
    return this.http.post('/api/Token',um,options);
  }
}
