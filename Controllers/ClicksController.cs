using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using urlstudio.Model;

namespace urlstudio.Controllers
{
    [Route("api/[controller]")]
    public class ClicksController : Controller
    {
        Repos _repos = new Repos();
       
        // GET: api/Clicks/customurl
        [HttpGet("{url}/{domain}")]
        public IEnumerable<ClickTracker> GetClickTracker(string url,string domain)
        {
            return _repos.GetClickRecords(url,domain);
        }

        //Post : api/Clicks
        [HttpPost]
        public void PostUrlMaster([FromBody] ClickTracker ct)
        {
            _repos.AddClick(ct);
        }
    }
}
