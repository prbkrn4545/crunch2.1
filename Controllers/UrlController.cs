using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using urlstudio.Model;

namespace urlstudio.Controllers
{
    [Route("api/[controller]")]
    public class UrlController : Controller
    {
        Repos _repos = new Repos();

        [HttpGet]
        public IEnumerable<UrlMaster> geturl()
        {
            return _repos.GetAll();
        }

        [HttpGet("{link}/{domain}")]
        public bool GetUrlMaster(string link, string domain)
        {
            return _repos.isExistingURL(link,domain);
        }


        [HttpGet("{link}/{domain}/{id1}/{id2}")]
        public IEnumerable<UrlMaster> getoriginalURL(string link,string domain)
        {
            IEnumerable<UrlMaster> urlm = (IEnumerable<UrlMaster>)_repos.GetOrigin(link,domain);
            if (urlm.Count() > 0)
            {
                _repos.UpdateClick(link,domain);
                return urlm;
            }
            else
                throw new ArgumentException("");
        }

        //service call == >>  getcount()
        [HttpGet("{link}/{domain}/{id2}")]
        public IEnumerable<UrlMaster> getcount(string link, string domain, int id2)
        {
            IEnumerable<UrlMaster> urlm = (IEnumerable<UrlMaster>)_repos.GetOrigin(link,domain);
            if (urlm.Count() > 0)
            {
                return urlm;
            }
            else
                throw new ArgumentException("");
        }

        //service call == >>  insert()
        [HttpPost]
        public void PostUrlMaster([FromBody] UrlMaster url)
        {
            _repos.Add(url);
        }
    }
}
