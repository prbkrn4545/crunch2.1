using System.Data;
using urlstudio.Model;
using System.Data.SqlClient;
using Dapper;
using System.Linq;
using System.Collections.Generic;
using System;
public class Repos
{
    // string myDb1ConnectionString = Configuration.GetConnectionString("DefaultConnection");
    private string connectionString;

    public Repos()
    {
        // connectionString = @"Server=DESKTOP-MVQ9FQC\SQLEXPRESS;Database=URLDB;Trusted_Connection=true;";
        connectionString = @"Data Source=SQL5053.site4now.net;Initial Catalog=DB_A62816_URLDB;User Id=DB_A62816_URLDB_admin;Password=qwerty098_;Trusted_Connection=False;";
    }

    public IDbConnection Connection
    {
        get
        {
            return new SqlConnection(connectionString);
        }
    }
    //this method adds the data into url master
    public void Add(UrlMaster urlm)
    {
        using (IDbConnection dbConnection = Connection)
        {
            try
            {
                var dt = DateTime.Now;
                var dt2 = dt.AddDays(3);
                string sQuery = "Insert into URLMaster values('" + urlm.OriginalUrl + "','" + urlm.AssignedUrl + "','" + dt.ToString("MM/dd/yyyy") + "','" + dt2.ToString("MM/dd/yyyy") + "','" + urlm.OptedServer + "'," + urlm.Clicks + ")";
                dbConnection.Open();
                dbConnection.Execute(sQuery, urlm);
            }
            catch (Exception)
            {
            }
        }
    }

    //add the visitor recoed into ClickTracker
    public void AddClick(ClickTracker ct)
    {
        using (IDbConnection dbConnection = Connection)
        {
            try
            {
                DateTime currentDateTime = DateTime.Now;
                // string sqlFormattedDate = currentDateTime.ToString("yyyy-MM-dd HH:mm:ss.fff");
                string sQuery = "INSERT into ClickTracker(CustomUrl,VisitedUser) values('" + ct.CustomUrl + "','" + ct.VisitedUser + "')";
                dbConnection.Open();
                dbConnection.Execute(sQuery, ct);
            }
            catch (Exception)
            {
            }

        }
    }

    //this method gets all the data in json format
    public IEnumerable<UrlMaster> GetAll()
    {
        using (IDbConnection dbConnection = Connection)
        {
            dbConnection.Open();
            return dbConnection.Query<UrlMaster>("SELECT * FROM UrlMaster");
        }
    }

    //this method checks whether the given url is existing ot not
    public bool isExistingURL(string desiredURL, string domain)
    {
        using (IDbConnection dbConnection = Connection)
        {
            dbConnection.Open();
            string qry = "SELECT * FROM UrlMaster WHERE  convert(varbinary, AssignedURL) = convert(varbinary, '" + desiredURL + "')  and OptedServer ='" + domain + "'";
            int count = dbConnection.Query<UrlMaster>(qry).ToList().Count;
            if (count > 0)
                return true;
            else
                return false;
        }
    }

    //this method brings the original URL that has been assigned to cutom URL
    public IEnumerable<UrlMaster> GetOrigin(string link,string domain)
    {
        using (IDbConnection dbConnection = Connection)
        {
            dbConnection.Open();
            return dbConnection.Query<UrlMaster>("Select * from URLMASTER where convert(varbinary, AssignedURL) = convert(varbinary, '" + link + "') and OptedServer = '"+domain+"'");
        }
    }
    
    //this method retreives the number of clicks on the custom url
    public int UpdateClick(string link,string domain)
    {
        using (IDbConnection dbConnection = Connection)
        {
            try
            {
                dbConnection.Open();
                return dbConnection.Query<UrlMaster>("update URLMASTER set Clicks = Clicks + 1 where convert(varbinary, AssignedURL) = convert(varbinary, '" + link + "')  and OptedServer = '"+domain+"'").Count();
            }
            catch (Exception)
            {
                return 0;
            }
        }
    }

    //Get all visitor from ClickTrackerTable based on custom URL
    public IEnumerable<ClickTracker> GetClickRecords(string link,string domain)
    {
        using (IDbConnection dbConnection = Connection)
        {
            dbConnection.Open();
            return dbConnection.Query<ClickTracker>("SELECT COUNT(ID) as total, VisitedUser from clickTracKer where convert(varbinary, CustomURL) = convert(varbinary, '" + link + "') and CustomDomain = '"+domain+"' GROUP BY VisitedUser");
        }
    }

    public bool isvaliduser()
    {
        using (IDbConnection dbConnection = Connection)
        {
            dbConnection.Open();
            int count = dbConnection.Query<UrlMaster>("SELECT * FROM UrlMaster").ToList().Count;
            if (count > 0)
                return true;
            else
                return false;
        }
    }
}