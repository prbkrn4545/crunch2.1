﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace urlstudio.Model
{
   public partial class URLDBContext : DbContext
    {
        public URLDBContext()
        {
        }

        public URLDBContext(DbContextOptions<URLDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ClickTracker> ClickTracker { get; set; }
        public virtual DbSet<UrlMaster> UrlMaster { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=DESKTOP-MVQ9FQC\\SQLEXPRESS;Database=URLDB;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ClickTracker>(entity =>
            {
                entity.HasKey(e => e.VisitedUser)
                    .HasName("PK_Click");

                entity.ToTable("clickTracker");

                entity.Property(e => e.VisitedUser)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CustomUrl)
                    .HasColumnName("CustomURL")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedOnAdd();

            });

            modelBuilder.Entity<UrlMaster>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AssignedOn).HasColumnType("date");

                entity.Property(e => e.AssignedUrl)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Clicks).HasDefaultValueSql("((0))");

                entity.Property(e => e.ExpiresOn).HasColumnType("date");

                entity.Property(e => e.OptedServer)
                    .HasColumnName("optedServer")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.OriginalUrl)
                    .HasMaxLength(300)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
