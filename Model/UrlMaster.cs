﻿using System;
using System.Collections.Generic;

namespace urlstudio.Model
{
    public partial class UrlMaster
    {
        public int Id { get; set; }
        public string OriginalUrl { get; set; }
        public string AssignedUrl { get; set; }
        public string AssignedOn { get; set; }
        public string ExpiresOn { get; set; }
        public string OptedServer { get; set; }
        
         public int? Clicks { get; set; }
    }
}
