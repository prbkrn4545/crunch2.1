﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace urlstudio.Model
{
    public partial class ClickTracker
    {
        public int Id { get; set; }
        public string CustomUrl { get; set; }
        
        public string VisitedUser { get; set; }
    }
}
